# HTTP Server

Java HTTP server written from scratch for my apprenticeship at [8th
Light](http://www.8thlight.com/). 

## Information

This project serves as an exercise for creating sockets, parsing requests, HTTP
protocol, and serving content. This project is also an exercise in the SOLID
principles.

Right now the server can only navigate to a designated port, and display "Hello,
world". However, by the end of the project it will meet these requirements:

  - HTTP Server from Scratch
  - Packaged and runnable on any computer
  - Can only use raw TCP sockets and thread classes/methods
  - Serve a Hello, World message in response to request at root
  - Point to a directory and serve files in that directory
  - Respond with appropriate error code if file doesn't exist
  - Can handle multiple requests
  
## Use

To use the jar file

1. Clone this directory with `$ git clone https://github.com/meaganewaller/http_server`
2. CD into the directory `$ cd http_server`
3. Run this `java -jar Start.jar -p [port number of your choice]` for example:
   `java -jar Start.jar -p 4000` will open a connection on port 4000. If you
   were to open your browser and go to localhost:4000 you should see the
   **Hello, world** message


