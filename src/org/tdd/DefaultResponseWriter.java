package org.tdd;

import java.io.IOException;
import java.io.OutputStream;

public class DefaultResponseWriter implements ResponseWriter {
	private OutputStream outputStream;

	public void send(HttpResponse response) throws IOException {
		outputStream.write(response.bytes());
	}

	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

}
