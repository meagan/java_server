package org.tdd;

public class RootResponse extends HttpResponse {
	public RootResponse() {}

	@Override
	public void buildResponse(HttpRequest request) {
		if(request.getMethod().equals("GET")) {
			setStatusLine("HTTP/1.1 200 OK");
			setBody(createHomePage());
		}
	}
	
	private String createHomePage() {
		FileManager fileManager = new DefaultFileManager("/Users/meaganewaller/apprenticeship/javaserver/cob_spec");

		StringBuilder homePage = new StringBuilder();
		
		homePage.append("<html><head></head><body>");
		homePage.append("<h1>Hello, world!</h1>");
		
		for (String file : fileManager.getPublicFileNames()) {
			homePage.append("<a href=\"/" + file + "\">" + file + "</a>\r\n<br />");
		}
		
		homePage.append("</body></html>");
		return homePage.toString();
	}

}
