package org.tdd;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class DefaultSocket implements Sockit {
	private Socket socket;
	
	public DefaultSocket(Socket defaultSocket) {
		socket = defaultSocket;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return socket.getOutputStream();
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return socket.getInputStream();
	}

	@Override
	public void close() throws IOException {
		socket.close();
	}
}
