package org.tdd;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class DefaultRequestReader implements RequestReader {
	private InputStream inputStream;
	private char[] buffer;
	private int pointer;

	public String readFromSocket() throws IOException {
		Reader input = new InputStreamReader(inputStream, "UTF-8");
		pointer = 0;
		

		buffer = new char[3000];

		while(readingHeaders()) {
			int atByte = input.read();
			buffer[pointer++] = (char) atByte;
		}

		if(hasBody()) {
			int contentLength = findContentLength();
			for(int i = 0; i < contentLength; i++) {
				int atByte = input.read();
				buffer[pointer++] = (char) atByte;
			}
		}
		
		return new String(buffer);
	}

	private boolean readingHeaders() {
		if(pointer < 0) {
			return false;
		}
		else {
			return !(pointer > 3 &&
					'\n' == buffer[pointer - 1] &&
					'\r' == buffer[pointer - 2] &&
					'\n' == buffer[pointer - 3] &&
					'\r' == buffer[pointer - 4]);
		}
	}

	private boolean hasBody() {
		return postOrPutRequest() && hasContentLength();
	}

	private boolean hasContentLength() {
		String headers = new String(buffer).trim();
		return headers.contains("Content-Length");
	}
	
	public int findContentLength() {
		return Integer.parseInt(new String(buffer).split("Content-Length")[1].split("\r\n")[0].split(":")[1].trim());
	}
	
	private boolean postOrPutRequest() {
		return buffer[0] == 'P';
	}

	@Override
	public void setInputStream(InputStream input) {
		inputStream = input;
	}
}