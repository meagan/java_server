package org.tdd;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public interface FileManager {
	public byte[] read(Path path) throws IOException;
	public File getPublicDirectory();
	public String[] getPublicFileNames();

}
