package org.tdd;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class ParametersResponse extends HttpResponse {
	public void buildResponse(HttpRequest request) {
		setStatusLine("HTTP/1.1 200 OK");
		setBody((getQueryString(request)));
	}
	
	private String getQueryString(HttpRequest request) {
		String splitURI = request.getURI().replaceAll("=", " = ");
		String[] uriStrings = splitURI.split("&");
		StringBuilder bodyData = new StringBuilder();
		for (String param : uriStrings ) {
			try {
				param = URLDecoder.decode(param.replace("/parameters?", ""), "UTF-8");
				bodyData.append(param + "\r\n");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return bodyData.toString();
	}

}
