package org.tdd;

public interface RequestHandler extends Runnable {
	public void run();
}
