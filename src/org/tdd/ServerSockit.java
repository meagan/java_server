package org.tdd;

import java.io.IOException;

public interface ServerSockit {
	public Sockit accept() throws IOException;
	public boolean isClosed();
}
