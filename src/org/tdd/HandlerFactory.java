package org.tdd;

import java.io.IOException;

public class HandlerFactory implements Handler {
	private Router serverRouter;
	
	public HandlerFactory(Router router) {
		serverRouter = router;
	}

	public DefaultRequestHandler makeHandler(Sockit socket) throws IOException {
		return new DefaultRequestHandler(socket, new DefaultRequestReader(), new DefaultResponseBuilder(serverRouter), new DefaultResponseWriter());
	}

}
