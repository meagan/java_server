package org.tdd;

import java.util.Hashtable;

public class HttpRequest {
	protected String fullHeaders;
	private String method;
	private String URI;
	private String httpVersion;
	public String body;
	public Hashtable<String, String> headers;

	public HttpRequest(String request) {
		body = "";
		fullHeaders = request.split("\r\n\r\n")[0];
		headers = new Hashtable<>();
		setRequestLine();
		if (hasHeaders()) {
			setHeaders();
		}
		if(((putRequest() || postRequest()) && (bodySentWith(request)))) {
			body = request.split("\r\n\r\n")[1];
		}
	}
	
	private boolean putRequest() {
		return method.equals("PUT");
	}
	
	private boolean postRequest() {
		return method.equals("POST");
	}
	
	private boolean bodySentWith(String request) {
		return request.split("\r\n\r\n").length > 1;
	}
	
	private void setRequestLine() {
		String requestLine = fullHeaders.split("r\n", 2)[0];
		method = requestLine.split(" ")[0];
		URI = requestLine.split(" ")[1];
		httpVersion = requestLine.split(" ")[2];
	}
	
	public String getMethod() {
		return method;
	}
	
	public String getURI() {
		return URI;
	}
	
	public String getHttpVersion() {
		return httpVersion;
	}
	
	private boolean hasHeaders() {
		return fullHeaders.split("\r\n").length > 1;
	}
	
	private void setHeaders() {
		String[] headersArray = fullHeaders.split("\r\n", 2)[1].split("\r\n");
		
		for(String header: headersArray) {
			String headerName = header.split(":")[0].toLowerCase();
			String headerContent = header.split(":")[1].trim();
			
			headers.put(headerName, headerContent);
		}
	}
	
	public String getContentLength() {
		return headers.get("content-length");
	}
	
	public String getAccept() {
		return headers.get("accept");
	}
	
	public String getRange() {
		return headers.get("range");
	}
	
	public String getAuthorization() {
		return headers.get("authorization");
	}
}
	
