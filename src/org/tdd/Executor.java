package org.tdd;

public interface Executor {
	public void execute(Runnable toRun);
}
