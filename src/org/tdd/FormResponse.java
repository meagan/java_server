package org.tdd;

public class FormResponse extends HttpResponse {

	public void buildResponse(HttpRequest request) {
		setStatusLine("HTTP/1.1 200 OK");

		if(putOrPost(request)) {
			String[] bodyData = request.body.split("=");
			setBody(bodyData[0] + " = " + bodyData[1]);
		}
		else if(delete(request)) {
			setBody("");
		}
	}
	
	private boolean putOrPost(HttpRequest request) {
		return request.getMethod().equals("POST") || request.getMethod().equals("PUT");
	}
	
	private boolean delete(HttpRequest request) {
		return request.getMethod().equals("DELETE");
	}

}
