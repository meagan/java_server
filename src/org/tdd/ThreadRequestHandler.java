package org.tdd;

public class ThreadRequestHandler implements RequestHandler {
	private RequestHandler handler;
	private Executor executor;

	public ThreadRequestHandler(RequestHandler requestHandler, Executor executor) {
		handler = requestHandler;
		this.executor = executor;
	}

	public void run() {
		executor.execute(handler);
	}
	
	public Executor getExecutor() {
		return executor;
	}

}
