package org.tdd;

import java.util.Hashtable;

public class DefaultResponseBuilder implements ResponseBuilder {
	private Hashtable<String, HttpResponse> routes;
	private HttpResponse response;
	private Router router;
	
	public DefaultResponseBuilder(Router theRouter) {
		router = theRouter;
		response = null;
		routes = router.getRoutes();
	}
	
	public HttpResponse buildResponse(HttpRequest request) {
		String path = request.getURI();

		if(path.contains("parameters?")) {
			response = new ParametersResponse();
		}
		else {
			response = routes.get(path);
			if(response == null) {
				response = new NotFoundResponse();
			}
		}

		response.buildResponse(request);
		return response;
	}
	
	public Router getRouter() {
		return router;
	}
}
