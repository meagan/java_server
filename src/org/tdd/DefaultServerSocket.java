package org.tdd;

import java.io.IOException;
import java.net.ServerSocket;

public class DefaultServerSocket implements ServerSockit {
	private ServerSocket serverSocket;

	public DefaultServerSocket(ServerSocket defaultServerSocket) {
		serverSocket = defaultServerSocket;
	}

	@Override
	public Sockit accept() throws IOException {
		return new DefaultSocket(serverSocket.accept());
	}

	@Override
	public boolean isClosed() {
		return serverSocket.isClosed();
	}

}
