package org.tdd;

import java.io.IOException;

public class DefaultRequestHandler implements RequestHandler{
	private ResponseBuilder responseBuilder;
	private ResponseWriter responseWriter;
	private RequestReader requestReader;
	private Sockit socket;

	public DefaultRequestHandler(Sockit socket, RequestReader requestReader, ResponseBuilder responseBuilder, ResponseWriter responseWriter) throws IOException {
		this.socket = socket;
		this.requestReader = requestReader;
		this.responseBuilder = responseBuilder;
		this.responseWriter = responseWriter;
		setReaderInputStream();
		setWriterOutputStream();
	}
	
	public void run() {
		try {
			String inputFromSocket = requestReader.readFromSocket();
			System.out.println("Yo, the input from the socket is" + inputFromSocket);
			HttpRequest request = new HttpRequest(inputFromSocket);
			HttpResponse response = responseBuilder.buildResponse(request);
			responseWriter.send(response);
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setReaderInputStream() throws IOException {
		requestReader.setInputStream(socket.getInputStream());
	}
	
	public void setWriterOutputStream() throws IOException {
		responseWriter.setOutputStream(socket.getOutputStream());
	}
	
	public ResponseWriter getResponseWriter() {
		return responseWriter;
	}
	
	public RequestReader getRequestReader() {
		return requestReader;
	}
	
	public ResponseBuilder getResponseBuilder() {
		return responseBuilder;
	}
	
	public Sockit getSocket() {
		return socket;
	}
}
