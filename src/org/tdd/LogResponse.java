package org.tdd;

import java.io.FileOutputStream;

public class LogResponse extends HttpResponse {
	@Override
	public void buildResponse(HttpRequest request) {
		FileOutputStream fileOutputStream = null;
		setStatusLine("HTTP/1.1 401 Unauthorized");
		try {
			fileOutputStream = new FileOutputStream("/Users/meaganewaller/apprenticeship/javaserver/cob_spec/public/logs", true);
			fileOutputStream.write(request.fullHeaders.getBytes());
		} catch (Exception e) {
			System.out.println("Couldn't create log");
		}
		
	}

}
