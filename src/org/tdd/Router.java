package org.tdd;

import java.util.Hashtable;

public interface Router {
	public Hashtable<String, HttpResponse> getRoutes();
}
