package org.tdd;

import java.util.LinkedList;

public abstract class HttpResponse {
	private String statusLine;
	private LinkedList<String> headers;
	byte[] body;

	public abstract void buildResponse(HttpRequest request);
	
	protected HttpResponse() {
		headers = new LinkedList<>();
		body = new byte[0];
	}

	public void setStatusLine(String status) {
		statusLine = status;
	}
	
	public String getStatusLine() {
		return statusLine;
	}
	
	public void setContentLength(int contentLength) {
		headers.add("Content-Length: " + contentLength);
	}
	
	public void setAccept(String accept) {
		headers.add("Accept: " + accept);
	}
	
	public void setLocation(String location) {
		headers.add("Location: " + location);
	}
	
	public void setContentType(String contentType) {
		headers.add("Content-Type: " + contentType);
	}
	
	public void setBody(byte[] bodyContent) {
		body = bodyContent;
	}
	
	@Override
	public String toString() {
		return getStringHeaders() + getStringBody();
	}
	
	public void setBody(String bodyContent) {
		body = bodyContent.getBytes();
	}
	
	public String getStringBody() {
		try {
			return new String(body);
		} catch (NullPointerException e) {
			return "";
		}
	}
	
	private String getStringHeaders() {
		StringBuilder responseBuilder = new StringBuilder();
		responseBuilder.append(statusLine + "\r\n");
		
		for (String header : headers) {
			responseBuilder.append(header + "\r\n");
		}
		responseBuilder.append("Allow: GET,HEAD,POST,OPTIONS,PUT,DELETE" + "\r\n");
		responseBuilder.append("\r\n");
		return responseBuilder.toString();
	}
	
	public byte[] bytes() {
		int headerLength = getStringHeaders().length();
		byte[] response = new byte[headerLength];
		if(body != null) {
			int responseLength = headerLength + body.length;
			response = new byte[responseLength];
			
			for (int h = 0; h < headerLength; h++) {
				response[h] = (byte) getStringHeaders().charAt(h);
			}
			for (int r = headerLength; r < responseLength; r++) {
				response[r] = body[r - headerLength];
			}
		}
		return response;
	}
}
