package org.tdd;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
	static Parser parser;
	static Integer port;
	static String directory;
	static FileManager fileManager;

	public static void main(String[] args) throws IOException {
		parser = new Parser(args);
		port = parser.portNumber();
		directory = parser.directory();

		ServerSocket userServerSocket = new ServerSocket(port);
		DefaultServerSocket serverSocket = new DefaultServerSocket(userServerSocket);
		
		fileManager = new DefaultFileManager(directory);
		
		ExecutorService executorService = Executors.newCachedThreadPool();
		Executor executor = new DefaultExecutorService(executorService);
		
		CobSpecRoutes router = new CobSpecRoutes(fileManager);
		HandlerFactory handler = new HandlerFactory(router);
		ThreadHandlerFactory threadHandlerFactory = new ThreadHandlerFactory(handler, executor);
		
		Server server = new Server(serverSocket, threadHandlerFactory);
		server.start();
	}
}
