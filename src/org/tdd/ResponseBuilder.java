package org.tdd;

public interface ResponseBuilder {
	public HttpResponse buildResponse(HttpRequest request);
	public Router getRouter();
}
