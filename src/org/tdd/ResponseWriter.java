package org.tdd;

import java.io.IOException;
import java.io.OutputStream;

public interface ResponseWriter {
	public void send(HttpResponse response) throws IOException;
	public void setOutputStream(OutputStream outputStream);
}
