package org.tdd;

import java.io.IOException;

public interface Handler {
	public RequestHandler makeHandler(Sockit socket) throws IOException;
}
