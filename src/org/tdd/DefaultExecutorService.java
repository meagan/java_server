package org.tdd;

import java.util.concurrent.ExecutorService;

public class DefaultExecutorService implements Executor {
	private ExecutorService executorService;
	
	public DefaultExecutorService(ExecutorService service) {
		executorService = service;
	}

	public void execute(Runnable toRun) {
		executorService.execute(toRun);
	}

}
