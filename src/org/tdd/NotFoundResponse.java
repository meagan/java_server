package org.tdd;

public class NotFoundResponse extends HttpResponse {
	public void buildResponse(HttpRequest request) {
		setStatusLine("HTTP/1.1 404 Not Found");
		setBody(createBody());
	}
	
	public String createBody() {
		StringBuilder body = new StringBuilder();
		body.append("<html><body>");
		body.append("<h1>404 Not Found</h1>");
		body.append("</html></body>");
		return body.toString();
	}

}
