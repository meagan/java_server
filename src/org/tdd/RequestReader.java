package org.tdd;

import java.io.IOException;
import java.io.InputStream;

public interface RequestReader {
	public String readFromSocket() throws IOException;

	public void setInputStream(InputStream inputStream);
}
