package org.tdd;

public class Parser {
	private String[] args;

	public Parser(String[] args) {
		this.args = args;
	}
	
	public int portNumber() {
		Integer portNumber;

		if((args.length > 1) && (args[0].equals("-p"))) {
			if(Integer.parseInt(args[1]) < 1024) {
				throw new RuntimeException("Pick a port 1024 or greater, please!");
			}
			else {
				portNumber = Integer.parseInt(args[1]);
			}
		}
		else {
			portNumber = 5000;
		}
		return portNumber;
	}
	
	public String directory() {
		String directory;
		
		if((args.length > 1) && (args[0].equals("-d"))) {
			directory = args[1];
		}
		else if((args.length > 2) && (args[2].equals("-d"))) {
			directory = args[3];
		}
		else {
			directory = "/Users/meaganewaller/apprenticeship/javaserver/cob_spec";
		}
		
		return directory;
	}

}
