package org.tdd;

import java.util.Hashtable;

public class CobSpecRoutes implements Router {
	private Hashtable<String, HttpResponse> routes;
	private FileManager fileManager;
	
	public CobSpecRoutes(FileManager fileManager) {
		this.fileManager = fileManager;
		routes = new Hashtable<>();
		initializeRoutes();
	}
	
	public Hashtable<String, HttpResponse> getRoutes() {
		return routes;
	}
	
	private void initializeRoutes() {
		routes.put("/", new RootResponse());
		routes.put("/redirect", new RedirectResponse());
		routes.put("/form", new FormResponse());
		routes.put("/parameters", new ParametersResponse());
		routes.put("/these", new LogResponse());
		routes.put("/log", new LogResponse());
		routes.put("/requests", new LogResponse());
		addFileRoutes();
	}
	
	private void addFileRoutes() {
		String[] fileNames = fileManager.getPublicFileNames();
		for (String name : fileNames) {
			name = "/" + name;
			routes.put(name, new FileResponse(fileManager, name));
		}
	}
}
