package org.tdd;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Sockit {
	public OutputStream getOutputStream() throws IOException;
	public InputStream getInputStream() throws IOException;
	
	public void close() throws IOException;
}
