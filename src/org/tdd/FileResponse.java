package org.tdd;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileResponse extends HttpResponse {
	private FileManager fileManager;
	private Path filePath;
	
	public FileResponse(FileManager files, String fileName) {
		fileManager = files;
		filePath = Paths.get(files.getPublicDirectory() + fileName);
	}

	@Override
	public void buildResponse(HttpRequest request) {
		if(methodOptions(request)) {
			goodResponse();
		}
		else if(anyMethodExceptGet(request)) {
			methodNotAllowedResponse();
		}
		else if(authResponse(request)) {
			if(authorized(request)) {
				goodResponse();
				try {
					setBody(readBodyFromFile(filePath));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				unauthorizedResponse();
				setBody("Authentication required");
			}
		}
		else {
			goodResponse();
			setImageContentType(request.getURI());
			byte[] body = null;
			try {
				body = readBodyFromFile(filePath);
				if(body != null) {
					setContentLength(body.length);
				}
			} catch(IOException e) {
				e.printStackTrace();
			}

			String range = request.getRange();
			if (range != null) {
				body = trimBodyToRange(new String(body), range).getBytes();
				partialContentResponse();
			}
			setBody(body);
		}
	}

	private void setImageContentType(String uri) {
		if(uri.contains("image.")) {
			setContentType("image/" + uri.split("\\.")[1]);
		}
	}

	private String trimBodyToRange(String body, String range) {
		int startingPoint = Integer.parseInt(range.split("=")[1].split("-")[0]);
		int endingPoint = Integer.parseInt(range.split("=")[1].split("-")[1]);
		return body.substring(startingPoint, endingPoint);
	}
	
	private boolean authorized(HttpRequest request) {
		boolean answer = false;
		if(request.getAuthorization() != null) {
			answer = request.getAuthorization().equals("Basic YWRtaW46aHVudGVyMg==");
		}
		return answer;
	}
	
	private boolean methodOptions(HttpRequest request) {
		return(request.getMethod().equals("HEAD") || request.getMethod().equals("OPTIONS"));
	}
	
	private boolean anyMethodExceptGet(HttpRequest request) {
		return(!request.getMethod().equals("GET"));
	}
	
	private boolean authResponse(HttpRequest request) {
		return(request.getURI().equals("/logs"));
	}
	
	private void goodResponse() {
		setStatusLine("HTTP/1.1 200 OK");
	}
	
	private void methodNotAllowedResponse() {
		setStatusLine("HTTP/1.1 405 Method Not Allowed");
	}
	
	private byte[] readBodyFromFile(Path filePath) throws IOException {
		return fileManager.read(filePath);
	}
	
	private void unauthorizedResponse() {
		setStatusLine("HTTP/1.1 401 Unauthorized");
	}
	
	private void partialContentResponse() {
		setStatusLine("HTTP/1.1 206 Partial Content");
	}
}
