package org.tdd;

import java.io.IOException;

public class Server {
	private ServerSockit serverSocket;
	private Handler handler;


	public Server(ServerSockit serverSocket, Handler handler) {
		this.serverSocket = serverSocket;
		this.handler = handler;
	}
	
	public void start() throws IOException {
		while(!serverSocket.isClosed()) {
			Sockit socket = serverSocket.accept();
			RequestHandler requestHandler = handler.makeHandler(socket);
			requestHandler.run();
		}
	}
}