package org.tdd;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DefaultFileManager implements FileManager {
	private String rootDirectory;
	
	public DefaultFileManager(String root) {
		rootDirectory = root;
	}

	public byte[] read(Path path) throws IOException {
		return Files.readAllBytes(path);
	}

	public File getPublicDirectory() {
		return new File(rootDirectory + "/public");
	}

	public String[] getPublicFileNames() {
		return getPublicDirectory().list();
	}

}
