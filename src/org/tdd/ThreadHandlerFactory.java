package org.tdd;

import java.io.IOException;

public class ThreadHandlerFactory implements Handler {
	private Handler handler;
	private Executor executor;
	
	public ThreadHandlerFactory(Handler handler, Executor executor) {;
		this.handler = handler;
		this.executor = executor;
	}
	
	public RequestHandler makeHandler(Sockit socket) throws IOException {
		RequestHandler requestHandler = handler.makeHandler(socket);
		return new ThreadRequestHandler(requestHandler, executor);
	}

}
