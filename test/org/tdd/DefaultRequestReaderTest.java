package org.tdd;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;

public class DefaultRequestReaderTest {
	private InputStream inputStream;
	private DefaultRequestReader reader;
	
	@Before
	public void setUp() throws Exception {
		inputStream = new ByteArrayInputStream("GET / HTTP/1.1\r\n\r\n".getBytes());
		reader = new DefaultRequestReader();
		reader.setInputStream(inputStream);
	}

	@Test
	public void itReadsFromClientConnection() throws IOException {
		assertThat(reader.readFromSocket(), containsString("GET / HTTP/1.1\r\n\r\n"));
	}
	
	@Test
	public void itStopsReadingAfterTheCRLFForGet() throws IOException {
		inputStream = new ByteArrayInputStream("GET /foobar HTTP/1.1\r\n\r\nmorestuff".getBytes());
		reader.setInputStream(inputStream);
		String socketInput = reader.readFromSocket();
		assertThat(socketInput, containsString("GET /foobar HTTP/1.1\r\n\r\n"));
		assertThat(socketInput, not(containsString("morestuff")));
	}
	
	@Test
	public void itCanFindTheContentLength() throws IOException {
		inputStream = new ByteArrayInputStream("POST /foobar HTTP/1.1\r\nContent-Length: 6\r\n\r\npretzel".getBytes());
		reader.setInputStream(inputStream);
		
		reader.readFromSocket();
		int contentLength = reader.findContentLength();
		assertEquals(6, contentLength);
	}
	
	@Test
	public void itReadsBodyForPostRequest() throws IOException {
		inputStream = new ByteArrayInputStream("POST /foobar HTTP/1.1\r\nContent-Length: 7\r\n\r\npretzel".getBytes());
		reader.setInputStream(inputStream);
		
		String socketInput = reader.readFromSocket();
		assertThat(socketInput, containsString("\r\n\r\npretzel"));
	}
	
	@Test
	public void itReadsBodyForPutRequest() throws IOException {
		inputStream = new ByteArrayInputStream("PUT /foobar HTTP/1.1\r\nContent-Length: 7\r\n\r\npretzel".getBytes());
		reader.setInputStream(inputStream);
		
		String socketInput = reader.readFromSocket();
		assertThat(socketInput, containsString("\r\n\r\npretzel"));
	}
}