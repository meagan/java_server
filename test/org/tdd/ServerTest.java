package org.tdd;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

public class ServerTest {
	private Server server;
	private MockServerSocket mockServerSocket;
	private MockRequestHandler mockRequestHandler;
	private MockHandlerFactory mockHandlerFactory;
	
	@Before
	public void setUp() throws IOException {
		mockServerSocket = new MockServerSocket();
		mockRequestHandler = new MockRequestHandler();
		mockHandlerFactory = new MockHandlerFactory();
		mockHandlerFactory.handler =  mockRequestHandler;
		
		server = new Server(mockServerSocket, mockHandlerFactory);
	}
	
	@Test
	public void itHandlesMultipleConnections() throws IOException {
		mockServerSocket.maxAccepts = 5;
		server.start();
		assertEquals(5, mockServerSocket.acceptCallCount);
	}
	
	@Test
	public void itDelegatesToHandlerFactory() throws IOException {
		mockServerSocket.maxAccepts = 5;
		server.start();
		assertEquals(5, mockHandlerFactory.makeHandlerCount);
	}
	
	@Test
	public void socketGetsSentToRequestHandler() throws IOException {
		mockServerSocket.maxAccepts = 1;
		server.start();
		assertSame(mockHandlerFactory.makeHandlerArgument, mockServerSocket.createdClientSocket);
	}
	
	@Test
	public void delegatesToRequestHandlerToRun() throws IOException {
		mockServerSocket.maxAccepts = 5;
		server.start();
		assertEquals(5, mockRequestHandler.runCallCount);
	}
}