package org.tdd;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class DefaultRequestHandlerTest {
	private MockSocket socket;
	private MockRequestReader requestReader;
	private MockResponseBuilder responseBuilder;
	private MockResponseWriter responseWriter;
	private DefaultRequestHandler requestHandler;
	
	@Before
	public void setUp() throws IOException {
		responseWriter = new MockResponseWriter();
		responseBuilder = new MockResponseBuilder();
		requestReader = new MockRequestReader();
		socket = new MockSocket();
		
		requestHandler = new DefaultRequestHandler(socket, requestReader, responseBuilder, responseWriter);
	}
	
	@Test
	public void itDelegatesRequestReadertoReadRequest() throws IOException {
		socket.mockInput = new ByteArrayInputStream("GET / HTTP/1.1\r\n\r\n".getBytes());
		requestHandler.setReaderInputStream();
		requestHandler.run();
		socket.mockInput = new ByteArrayInputStream("GET / HTTP/1.1\r\n\r\n".getBytes());
		requestHandler.setReaderInputStream();
		requestHandler.run();
		assertEquals(2, requestReader.readFromSocketCallCount);
	}
	
	@Test
	public void itDeletesResponseWriterToSendResponse() throws IOException {
		socket.mockInput = new ByteArrayInputStream("GET / HTTP/1.1\r\n\r\n".getBytes());
		requestHandler.setReaderInputStream();
		requestHandler.run();
		socket.mockInput = new ByteArrayInputStream("GET / HTTP/1.1\r\n\r\n".getBytes());
		requestHandler.setReaderInputStream();
		requestHandler.run();
		assertEquals(2, responseWriter.sendCallCount);
	}
	
	@Test
	public void itSendsParsedRequestToResponseBuilder() throws IOException {
		socket.mockInput = new ByteArrayInputStream("GET / HTTP/1.1\r\n\r\n".getBytes());
		requestHandler.setReaderInputStream();
		requestHandler.run();
		HttpRequest parsed = responseBuilder.buildResponseArgument;
		assertEquals("GET", parsed.getMethod());
		assertEquals("/", parsed.getURI());
		assertEquals("HTTP/1.1", parsed.getHttpVersion());
	}
	
	@Test
	public void itSetsWriterOutputStreamFromSocket() throws IOException {
		requestHandler = new DefaultRequestHandler(socket, requestReader, responseBuilder, responseWriter);
		requestHandler = new DefaultRequestHandler(socket, requestReader, responseBuilder, responseWriter);
		assertEquals(3, responseWriter.setOutputStreamCallCount);
		assertEquals(3, socket.getOutputStreamCallCount);
	}
	
	@Test
	public void itSetsReaderInputStreamFromSocket() throws IOException {
		requestHandler = new DefaultRequestHandler(socket, requestReader, responseBuilder, responseWriter);
		requestHandler = new DefaultRequestHandler(socket, requestReader, responseBuilder, responseWriter);
		assertEquals(3, requestReader.setInputStreamCallCount);
		assertEquals(3, socket.getInputStreamCallCount);
	}
	
	@Test
	public void itClosesSocketWhenDone() throws IOException {
		socket.mockInput = new ByteArrayInputStream("GET / HTTP/1.1\r\n\r\n".getBytes());
		requestHandler.setReaderInputStream();
		requestHandler.run();
		socket.mockInput = new ByteArrayInputStream("GET / HTTP/1.1\r\n\r\n".getBytes());
		requestHandler.setReaderInputStream();
		requestHandler.run();
		assertEquals(2, socket.closeCallCount);
	}
	
}
