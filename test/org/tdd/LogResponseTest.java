package org.tdd;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LogResponseTest {
	@Test
	public void itSetsUnauthorizedStatus() {
		HttpRequest request = new HttpRequest("GET /logs HTTP/1.1\r\n\r\n");
		HttpResponse response = new LogResponse();
		response.buildResponse(request);
		assertEquals("HTTP/1.1 401 Unauthorized", response.getStatusLine());
	}
}
