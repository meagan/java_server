package org.tdd;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.instanceOf;

import static org.hamcrest.core.Is.*;


import org.junit.Before;
import org.junit.Test;

public class DefaultResponseBuilderTest {
	private DefaultResponseBuilder builder;
	private MockRouter mockRouter;
	
	@Before
	public void setUp() {
		mockRouter = new MockRouter();
		builder = new DefaultResponseBuilder(mockRouter);
	}
	
	@Test
	public void itGetsRoutesFromRouter() {
		builder = new DefaultResponseBuilder(mockRouter);
		builder = new DefaultResponseBuilder(mockRouter);
		assertEquals(3, mockRouter.getRoutesCallCount);
	}
	
	@Test
	public void itIsNotFoundResponseIfUnknownRoute() {
		HttpRequest request = new HttpRequest("GET /rioanoa HTTP/1.1\r\n\r\n");
		HttpResponse response = builder.buildResponse(request);
		assertThat(response, is(instanceOf(NotFoundResponse.class)));
	}
	
	@Test
	public void createsParametersResponse() {
		HttpRequest request = new HttpRequest("GET /parameters?data=something HTTP/1.1\r\n\r\n");
		HttpResponse response = builder.buildResponse(request);
		assertThat(response, is(instanceOf(ParametersResponse.class)));
	}
}
