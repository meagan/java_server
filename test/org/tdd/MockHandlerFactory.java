package org.tdd;

import java.io.IOException;

public class MockHandlerFactory implements Handler {
	public int makeHandlerCount;
	public MockRequestHandler handler;
	public Sockit makeHandlerArgument;
	
	public MockHandlerFactory() {
		makeHandlerCount = 0;
		handler = new MockRequestHandler();
	}

	public RequestHandler makeHandler(Sockit socket) throws IOException {
		makeHandlerArgument = socket;
		makeHandlerCount++;
		return handler;
	}
}
