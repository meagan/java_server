package org.tdd;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class HttpResponseTest {
	private HttpResponse response;
	
	@Before
	public void setUp() {
		response = new RootResponse();
	}
	
	@Test
	public void itHasAStatusLine() {
		response.setStatusLine("HTTP/1.1 200 OK");
		assertThat(response.getStatusLine(), containsString("HTTP/1.1 200 OK"));
	}
	
	@Test
	public void itReturnsResponseAsStrings() {
		response.setContentLength(15);
		response.setAccept("bytes=0-2");
		response.setLocation("http://somewhere.com/");
		String fullResponse = response.toString();
		assertThat(fullResponse, containsString("Content-Length: 15\r\n"));
		assertThat(fullResponse, containsString("Accept: bytes=0-2\r\n"));
		assertThat(fullResponse, containsString("Location: http://somewhere.com/\r\n"));
	}
	
	@Test
	public void responseCanContainBody() {
		response.setStatusLine("HTTP/1.1 200 OK");
		response.setBody("hello, world!".getBytes());
		String fullResponse = response.toString();
		assertThat(fullResponse, containsString("HTTP/1.1 200 OK\r\n"));
		assertThat(fullResponse, containsString("\r\n\r\nhello, world!"));
	}
	
	@Test
	public void returnsAnEmptyStringForNoBody() {
		assertEquals("", response.getStringBody());
	}
	
	@Test
	public void responseCanBeInBytes() {
		response.setStatusLine("HTTP/1.1 200 OK");
		response.setBody("howdy".getBytes());
		int responseLength = response.toString().length();
		assertEquals(responseLength, response.bytes().length);
	}

	@Test
	public void setsContentType() {
		response.setContentType("text/html");
		assertThat(response.toString(), containsString("text/html"));
	}
	
	@Test
	public void returnsEmptyStringIfNullBody() {
		assertEquals("", response.getStringBody());
	}
	
	@Test
	public void returnsBytesWhenBodyNotNull() {
		response.setContentType("text/html");
		response.setBody("hello world!".getBytes());
		assertEquals(86, response.bytes().length);
	}
	
	@Test
	public void returnsBytesWhenBodyIsNull() {
		response.setContentType("text/html");
		response.setBody("".getBytes());
		assertEquals(74, response.bytes().length);
	}
}
