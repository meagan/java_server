package org.tdd;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Hashtable;

public class MockFileManager implements FileManager {
	public int readFilePathCallCount;
	public Path readFilePathArgument;
	public Hashtable<String, byte[]> files;

	public MockFileManager() {
		readFilePathCallCount = 0;
		files = new Hashtable<>();
	}

	@Override
	public byte[] read(Path path) throws IOException {
		readFilePathCallCount++;
		readFilePathArgument = path;
		String[] splitPath = path.toString().split("/");
		String fileName = "/" + splitPath[splitPath.length - 1];
		return files.get(fileName);
	}
	
	public void addFile(String name, String content) {
		files.put("/" + name, content.getBytes());
	}

	@Override
	public File getPublicDirectory() {
		return new File("/Users/meaganewaller/apprenticeship/javaserver/httpserver/test/org/tdd/public/");
	}

	@Override
	public String[] getPublicFileNames() {
		String[] fileNames = new String[files.size()];
		int index = 0;
		
		for (String name : files.keySet()) {
			fileNames[index] = fileWithoutFirstSlash(name);
			index++;
		}
		
		return fileNames;
	}
	
	private String fileWithoutFirstSlash(String name) {
		return name.substring(1);
	}

}
