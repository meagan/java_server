package org.tdd;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.Test;

public class RedirectResponseTest {
	@Test
	public void itRedirectsToRootPath() {
		HttpRequest request = new HttpRequest("GET /redirect HTTP/1.1\r\n\r\n");
		HttpResponse response = new RedirectResponse();
		response.buildResponse(request);
		
		assertEquals("HTTP/1.1 301 Moved Permanently", response.getStatusLine());
		assertThat(response.toString(), containsString("Location: http://localhost:5000/"));
	}

}
