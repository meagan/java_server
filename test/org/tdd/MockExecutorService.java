package org.tdd;

public class MockExecutorService implements Executor {
	public int executeCallCount;
	public Runnable executeArgument;
	
	public MockExecutorService() {
		executeCallCount = 0;
		executeArgument = null;
	}
	
	public void execute(Runnable runnable) {
		executeCallCount++;
		executeArgument = runnable;
	}

}
