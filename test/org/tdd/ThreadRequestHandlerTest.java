package org.tdd;

import static org.junit.Assert.*;

import org.junit.Test;

public class ThreadRequestHandlerTest {
	@Test
	public void itUsesExecutorToRunRequest() {
		MockRequestHandler mockRequestHandler = new MockRequestHandler();
		MockExecutorService mockExecutor = new MockExecutorService();
		ThreadRequestHandler handler = new ThreadRequestHandler(mockRequestHandler, mockExecutor);
		handler.run();
		handler.run();
		handler.run();
		
		assertEquals(3, mockExecutor.executeCallCount);
		assertEquals(mockRequestHandler, mockExecutor.executeArgument);
	}

}
