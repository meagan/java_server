package org.tdd;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.junit.Test;

public class DefaultResponseWriterTest {
	private OutputStream output;
	private DefaultResponseWriter responder;
	
	@Test
	public void itWritesResponseToOutputStream() throws IOException {
		output = new ByteArrayOutputStream();
		responder = new DefaultResponseWriter();
		responder.setOutputStream(output);
		HttpResponse response = new RootResponse();
		response.setStatusLine("HTTP/1.1 200 OK");
		response.setContentLength(16);
		response.setBody("java http server");
		
		responder.send(response);
		
		String sentResponse = output.toString();
		assertEquals("HTTP/1.1 200 OK\r\nContent-Length: 16\r\nAllow: GET,HEAD,POST,OPTIONS,PUT,DELETE\r\n\r\njava http server", sentResponse);
	}

}
