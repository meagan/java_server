package org.tdd;

public class MockRequestHandler implements RequestHandler {
	public int runCallCount;
	
	public MockRequestHandler() {
		runCallCount = 0;
	}

	@Override
	public void run() {
		runCallCount++;
	}

}
