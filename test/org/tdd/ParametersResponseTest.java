package org.tdd;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import org.junit.Test;

public class ParametersResponseTest {
	
	@Test
	public void itReturns200StatusLine() {
		HttpRequest request = new HttpRequest("GET /parameters?something=somethingelse&nothing=nothing HTTP/1.1\r\n\r\n");
		HttpResponse response = new ParametersResponse();
		response.buildResponse(request);
		assertEquals("HTTP/1.1 200 OK", response.getStatusLine());
	}
	
	@Test
	public void itDecodesParams() {
		HttpRequest request = new HttpRequest("GET /parameters?something=somethingelse&nothing=nothing HTTP/1.1\r\n\r\n");
		HttpResponse response = new ParametersResponse();
		response.buildResponse(request);
		assertThat(response.getStringBody(), containsString("something = somethingelse"));
		assertThat(response.getStringBody(), containsString("nothing = nothing"));
	}
	
	@Test
	public void itDecodesWeirdParams() {
		HttpRequest request = new HttpRequest("GET /parameters?variable_1=Operators%20%3C%2C%20%3E%2C%20%3D%2C%20!%3D%3B%20%2B%2C%20-%2C%20*%2C%20%26%2C%20%40%2C%20%23%2C%20%24%2C%20%5B%2C%20%5D%3A%20%22is%20that%20all%22%3F&variable_2=stuff HTTP/1.1\r\n\r\n");
		HttpResponse response = new ParametersResponse();
		response.buildResponse(request);
		StringBuilder expected = new StringBuilder();
		expected.append("variable_1 = Operators <, >, =, !=; +, -, *, &, @, #, $, [, ]: \"is that all\"?");
		expected.append("\r\n");
		expected.append("variable_2 = stuff");
		expected.append("\r\n");
		String expectedString = expected.toString();
		assertEquals(expectedString, response.getStringBody());
	}
}
