package org.tdd;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class HttpRequestTest {
	private HttpRequest request;
	
	@Test
	public void itGetsTheMethod() {
		request = new HttpRequest("GET / HTTP/1.1\r\n\r\n");
		assertEquals("GET", request.getMethod());
	}
	
	@Test
	public void itGetsTheURI() {
		request = new HttpRequest("GET / HTTP/1.1\r\n\r\n");
		assertEquals("/", request.getURI());
	}
	
	@Test
	public void itGetsComplexURI() {
		request = new HttpRequest("GET /foobar/foobaz/1 HTTP/1.1\r\n\r\n");
		assertEquals("/foobar/foobaz/1", request.getURI());
	}
	
	@Test
	public void onlyAddstoBodyWithSomethingInPost() {
		request = new HttpRequest("POST /foo HTTP/1.1\r\n\r\n");
		assertEquals("", request.body);
	}
	
	@Test
	public void itGetsHttpVersion() {
		request = new HttpRequest("GET / HTTP/1.1\r\n\r\n");
		assertEquals("HTTP/1.1", request.getHttpVersion());
	}
	
	@Test
	public void itGetsCorrectNumberOfHeaders() {
		request = new HttpRequest("GET / HTTP/1.1\r\nAccept: text/html\r\nContent-Length: 4\r\n\r\nhello");
		assertEquals(2, request.headers.size());
	}
	
	@Test
	public void itGetsHeaders() {
		request = new HttpRequest("GET / HTTP/1.1\r\nAccept: text/html\r\nContent-Length: 4\r\n\r\nhello");
		assertEquals("4", request.getContentLength());
		assertEquals("text/html", request.getAccept());
	}
	
	@Test
	public void readsBodyForPostRequest() {
		request = new HttpRequest("POST / HTTP/1.1\r\nAccept: text/html\r\nContent-Length: 4\r\n\r\njava\r\n\r\n");
		assertThat(request.body, containsString("java"));
	}
	
	@Test
	public void itGetsRange() {
		request = new HttpRequest("POST / HTTP/1.1\r\nAccept: text/html\r\nContent-Length: 4\r\nRange: bytes=0-2\r\n\r\nheyyy\r\n\r\n");
		assertEquals("bytes=0-2", request.getRange());
	}
	
	@Test
	public void itGetsAuthorization() {
		request = new HttpRequest("GET / HTTP/1.1\r\nAuthorization: thisistheauthorization\r\n\r\n");
		assertEquals("thisistheauthorization", request.getAuthorization());
	}
}