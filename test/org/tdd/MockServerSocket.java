package org.tdd;

import java.io.IOException;

public class MockServerSocket implements ServerSockit {
	public int maxAccepts;
	public int acceptCallCount;
	public Sockit createdClientSocket;
	private boolean isClosed;

	@Override
	public Sockit accept() throws IOException {
		acceptCallCount++;
		if(acceptCallCount >= maxAccepts) {
			isClosed = true;
		}
		
		return createdClientSocket;

	}

	@Override
	public boolean isClosed() {
		return isClosed;
	}

}
