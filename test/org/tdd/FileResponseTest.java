package org.tdd;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Before;
import org.junit.Test;

public class FileResponseTest {
	MockFileManager fakeFiles;
	HttpResponse fileResponse;
	HttpRequest request;
	
	@Before
	public void setUp() {
		fakeFiles = new MockFileManager();
		fakeFiles.addFile("test", "file 1 contents");
		fakeFiles.addFile("logs", "");
		fileResponse = new FileResponse(fakeFiles, "/test");
	}
	
	@Test
	public void itBuildsResponseIfMethodOptionsHead() {
		request = new HttpRequest("HEAD /test HTTP/1.1\r\n\r\n");
		fileResponse.buildResponse(request);
		assertEquals("HTTP/1.1 200 OK", fileResponse.getStatusLine());
	}
	
	@Test
	public void itBuildsResponseIfMethodOptionsOptions() {
		request = new HttpRequest("OPTIONS /test HTTP/1.1\r\n\r\n");
		fileResponse.buildResponse(request);
		assertEquals("HTTP/1.1 200 OK", fileResponse.getStatusLine());
	}
	
	@Test
	public void itBuildsResponseIfNotGet() {
		request = new HttpRequest("PUT /test HTTP/1.1\r\n\r\n");
		fileResponse.buildResponse(request);
		assertEquals("HTTP/1.1 405 Method Not Allowed", fileResponse.getStatusLine());
	}
	
	@Test
	public void itBuildsResponseIfUnAuthorized() {
		request = new HttpRequest("GET /logs HTTP/1.1\r\n\r\n");
		fileResponse.buildResponse(request);
		assertEquals("HTTP/1.1 401 Unauthorized", fileResponse.getStatusLine());
	}
	
	@Test
	public void itBuildsResponseIfAuthorized() {
		request = new HttpRequest("GET /logs HTTP/1.1\r\nAuthorization: Basic YWRtaW46aHVudGVyMg==\r\n\r\n");
		fileResponse.buildResponse(request);
		assertEquals("HTTP/1.1 200 OK", fileResponse.getStatusLine());
	}
	
	@Test
	public void buildsAllOtherResponses() {
		request = new HttpRequest("GET /file1 HTTP/1.1\r\n\r\n");
		fileResponse.buildResponse(request);
		assertEquals("HTTP/1.1 200 OK", fileResponse.getStatusLine());
	}

	@Test
	public void itSetsContentLengthFromBody() {
		request = new HttpRequest("GET /file1 HTTP/1.1\r\n\r\n");
		fileResponse.buildResponse(request);
		assertThat(fileResponse.toString(), containsString("Content-Length: 15"));
	}
	
	@Test
	public void itSetsPartialContentResponse() {
		request = new HttpRequest("GET /file1 HTTP/1.1\r\nRange: bytes=0-2\r\n\r\n");
		fileResponse.buildResponse(request);
		assertEquals("HTTP/1.1 206 Partial Content", fileResponse.getStatusLine());
	}
	
	@Test
	public void itSetsPartialContentBody() {
		request = new HttpRequest("GET /file1 HTTP/1.1\r\nRange: bytes=0-2\r\n\r\n");
		fileResponse.buildResponse(request);
		assertEquals("fi", fileResponse.getStringBody());
	}
	
	@Test
	public void itSetsImageContentType() {
		request = new HttpRequest("GET /image.png HTTP/1.1\r\n\r\n");
		fileResponse.buildResponse(request);
		assertThat(fileResponse.toString(), containsString("Content-Type: image/png"));
	}
}
