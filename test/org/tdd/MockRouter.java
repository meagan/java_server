package org.tdd;

import java.util.Hashtable;

public class MockRouter implements Router {
	public int getRoutesCallCount;
	private Hashtable<String, HttpResponse> routes;
	
	public MockRouter() {
		getRoutesCallCount = 0;
		routes = new Hashtable<>();
	}

	public Hashtable<String, HttpResponse> getRoutes() {
		getRoutesCallCount++;
		return routes;
	}

}
