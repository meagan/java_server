package org.tdd;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.containsString;


import org.junit.Test;

public class RootResponseTest {
	@Test
	public void returns200Status() {
		HttpRequest request = new HttpRequest("GET / HTTP/1.1\r\n\r\n");
		HttpResponse response = new RootResponse();
		response.buildResponse(request);
		assertEquals("HTTP/1.1 200 OK", response.getStatusLine());
	}
	
	@Test
	public void itShowsDirectoryLinks() {
		HttpRequest request = new HttpRequest("GET / HTTP/1.1\r\n\r\n");
		HttpResponse response = new RootResponse();
		response.buildResponse(request);
		assertThat(response.getStringBody(), containsString("<a href=\"/file1\">file1</a>"));
		assertThat(response.getStringBody(), containsString("<a href=\"/file2\">file2</a>"));
		assertThat(response.getStringBody(), containsString("<a href=\"/image.gif\">image.gif</a>"));
		assertThat(response.getStringBody(), containsString("<a href=\"/image.jpeg\">image.jpeg</a>"));
		assertThat(response.getStringBody(), containsString("<a href=\"/image.png\">image.png</a>"));
		assertThat(response.getStringBody(), containsString("<a href=\"/text-file.txt\">text-file.txt</a>"));
		assertThat(response.getStringBody(), containsString("<a href=\"/partial_content.txt\">partial_content.txt</a>"));
	}

}
