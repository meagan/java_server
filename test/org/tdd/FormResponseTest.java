package org.tdd;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class FormResponseTest {
	
	@Test
	public void itGeneratesStatus200Response() {
		HttpRequest request = new HttpRequest("GET /form HTTP/1.1\r\n\r\n");
		HttpResponse response = new FormResponse();
		response.buildResponse(request);
		assertEquals("HTTP/1.1 200 OK", response.getStatusLine());
	}
	
	@Test
	public void itAddsToBodyIfPostRequest() {
		HttpRequest request = new HttpRequest("POST /form HTTP/1.1\r\n\r\ndata=something");
		HttpResponse response = new FormResponse();
		response.buildResponse(request);
		assertEquals("HTTP/1.1 200 OK", response.getStatusLine());
		assertThat(response.getStringBody(), containsString("data = something"));
	}
	
	@Test
	public void itAddsToBodyIfPutRequest() {
		HttpRequest request = new HttpRequest("PUT /form HTTP/1.1\r\n\r\ndata=something");
		HttpResponse response = new FormResponse();
		response.buildResponse(request);
		assertEquals("HTTP/1.1 200 OK", response.getStatusLine());
		assertThat(response.getStringBody(), containsString("data = something"));
	}
	
	@Test
	public void itDeletesWithDeleteRequest() {
		HttpRequest request = new HttpRequest("DELETE /form HTTP/1.1\r\n\r\n");
		HttpResponse response = new FormResponse();
		response.buildResponse(request);
		assertThat(response.getStringBody(), containsString(""));
	}
}
