package org.tdd;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MockSocket implements Sockit {
	public InputStream mockInput;
	public OutputStream mockOutput;
	public int getOutputStreamCallCount;
	public int getInputStreamCallCount;
	public int closeCallCount;
	
	public MockSocket() {
		mockInput = new ByteArrayInputStream("mock socket".getBytes());
		getOutputStreamCallCount = 0;
		getInputStreamCallCount = 0;
		closeCallCount = 0;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		getOutputStreamCallCount++;
		return mockOutput;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		getInputStreamCallCount++;
		return mockInput;
	}

	@Override
	public void close() throws IOException {
		closeCallCount++;
	}
}
