package org.tdd;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import java.util.Hashtable;

import org.junit.Before;
import org.junit.Test;

public class CobSpecRoutesTest {
	private Hashtable<String, HttpResponse> routes;
	MockFileManager fakeFiles;
	
	@Before
	public void setUp() {
		fakeFiles = new MockFileManager();
		fakeFiles.addFile("file1", "file 1 contents");
		fakeFiles.addFile("file2", "file 2 contents");
		fakeFiles.addFile("image.jpeg", "jpeg contents");
		fakeFiles.addFile("image.png", "png contents");
		fakeFiles.addFile("image.gif", "gif contents");
		fakeFiles.addFile("partial_content.txt", "partial content contents");
		fakeFiles.addFile("text-file.txt", "text file contents");
		CobSpecRoutes router = new CobSpecRoutes(fakeFiles);
		routes = router.getRoutes();
	}
	
	@Test
	public void itHasRootRoute() {
		HttpResponse response = routes.get("/");
		assertNotNull(response);
	}
	
	@Test
	public void itHasRedirectRoute() {
		HttpResponse response = routes.get("/redirect");
		assertNotNull(response);
		assertThat(response, is(instanceOf(RedirectResponse.class)));
	}
	
	@Test
	public void itHasFormRoute() {
		HttpResponse response = routes.get("/form");
		assertNotNull(response);
		assertThat(response, is(instanceOf(FormResponse.class)));
	}
	
	@Test
	public void itHasFileRoute() {
		HttpResponse response = routes.get("/file1");
		assertNotNull(response);
		assertThat(response, is(instanceOf(FileResponse.class)));
	}
	
	@Test
	public void itHasImageRoute() {
		HttpResponse response = routes.get("/image.png");
		assertNotNull(response);
		assertThat(response, is(instanceOf(FileResponse.class)));
	}
	
	@Test
	public void itHasTextRoute() {
		HttpResponse response = routes.get("/text-file.txt");
		assertNotNull(response);
		assertThat(response, is(instanceOf(FileResponse.class)));
	}
	
	@Test
	public void itHasPartialContentRoute() {
		HttpResponse response = routes.get("/partial_content.txt");
		assertNotNull(response);
		assertThat(response, is(instanceOf(FileResponse.class)));
	}
	
	@Test
	public void itHasParametersRoute() {
		HttpResponse response = routes.get("/parameters");
		assertNotNull(response);
		assertThat(response, is(instanceOf(ParametersResponse.class)));
	}
}
