package org.tdd;

import java.io.IOException;
import java.io.OutputStream;

public class MockResponseWriter implements ResponseWriter {
	public int sendCallCount;
	public int setOutputStreamCallCount;
	
	public MockResponseWriter() {
		sendCallCount = 0;
		setOutputStreamCallCount = 0;
	}

	@Override
	public void send(HttpResponse response) throws IOException {
		sendCallCount++;
	}

	@Override
	public void setOutputStream(OutputStream outputStream) {
		setOutputStreamCallCount++;
	}
}
