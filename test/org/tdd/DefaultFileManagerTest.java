package org.tdd;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.junit.Test;

public class DefaultFileManagerTest {
	DefaultFileManager fileManager;
	
	@Test
	public void itHasPublicDirectory() {
		fileManager = new DefaultFileManager("rootpath");
		
		File publicDir = fileManager.getPublicDirectory();
		assertThat(publicDir, instanceOf(File.class));
	}
	
	@Test
	public void itReadsFromPath() throws IOException {
		Path path = (Path) FileSystems.getDefault().getPath("/Users/meaganewaller/apprenticeship/javaserver/httpserver/test/org/tdd/public", "test");
		fileManager = new DefaultFileManager("rootpath");
		assertEquals(14, fileManager.read(path).length);
	}
}
