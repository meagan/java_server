package org.tdd;

import java.io.IOException;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;

import org.junit.Before;
import org.junit.Test;

public class HandlerFactoryTest {
	private MockRouter mockRouter;
	private HandlerFactory factory;
	
	@Before
	public void setUp() { 
		mockRouter = new MockRouter();
		factory = new HandlerFactory(mockRouter);
	}
	
	@Test
	public void itCreatesRequestHandler() throws IOException {
		MockSocket mockSocket = new MockSocket();
		DefaultRequestHandler handler = factory.makeHandler(mockSocket);
		assertThat(handler, is(instanceOf(DefaultRequestHandler.class)));
		assertThat(handler.getRequestReader(), is(instanceOf(DefaultRequestReader.class)));
		assertThat(handler.getResponseWriter(), is(instanceOf(DefaultResponseWriter.class)));
		assertThat(handler.getResponseBuilder(), is(instanceOf(DefaultResponseBuilder.class)));
		assertSame(handler.getSocket(), mockSocket);
	}
	
	@Test
	public void itCreatesFileSystemForRouter() throws IOException {
		HandlerFactory factory = new HandlerFactory(mockRouter);
		DefaultRequestHandler handler = factory.makeHandler(new MockSocket());
		ResponseBuilder builder = handler.getResponseBuilder();
		Router router = builder.getRouter();
		assertSame(mockRouter, router);
	}

}
