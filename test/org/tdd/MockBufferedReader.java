package org.tdd;

import java.io.BufferedReader;
import java.io.Reader;

public class MockBufferedReader extends BufferedReader {

	private String mockInput[] = new String[50];
	private char[] mockReadInput;
	private int inputLineCount = 0;
	private int readLineCount = 0;

	public MockBufferedReader(Reader reader) {
		super(reader);
	}

	public void setMockInput(String str) {
		mockInput[inputLineCount++] = str;
	}

	public String readLine() {
		return mockInput[readLineCount++];
	}

	public int read(char[] cbuf, int offset, int end) {
		cbuf = mockReadInput;
		return end;
	}

	public void setMockReadInput(String input) {
		mockReadInput = input.toCharArray();
	}
}