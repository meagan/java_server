package org.tdd;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.Is.is;



import java.io.IOException;

import org.junit.Test;

public class ThreadHandlerFactoryTest {
	@Test
	public void itUsesHandlerToCreateHandlers() throws IOException {
		MockExecutorService mockExecutor = new MockExecutorService();
		MockHandlerFactory nonThreadFactory = new MockHandlerFactory();
		MockSocket mockSocket = new MockSocket();
		ThreadHandlerFactory threadFactory = new ThreadHandlerFactory(nonThreadFactory, mockExecutor);
		
		threadFactory.makeHandler(mockSocket);
		threadFactory.makeHandler(mockSocket);
		threadFactory.makeHandler(mockSocket);
		assertEquals(3, nonThreadFactory.makeHandlerCount);
	}
	
	@Test
	public void itCreatesThreadHandler() throws IOException {
		MockExecutorService mockExecutor = new MockExecutorService();
		MockHandlerFactory nonThreadFactory = new MockHandlerFactory();
		MockSocket mockSocket = new MockSocket();
		ThreadHandlerFactory threadFactory = new ThreadHandlerFactory(nonThreadFactory, mockExecutor);
		
		ThreadRequestHandler createdHandler = (ThreadRequestHandler) threadFactory.makeHandler(mockSocket);
		assertThat(createdHandler, is(instanceOf(ThreadRequestHandler.class)));
		assertSame(createdHandler.getExecutor(), mockExecutor);
	}

}
