package org.tdd;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ParserTest {
	private Parser parser;
	
	@Test
	public void itGetsPortFromCommandLineArgs() {
		String[] args = { "-p", "5000" }; 
		parser = new Parser(args);
		assertEquals(5000, parser.portNumber());
	}
	
	@Test
	public void defaultsToPort5000WhenNoArgs() {
		String[] args =  new String[0];
		parser = new Parser(args);
		assertEquals(5000, parser.portNumber());
	}
	
	@Test
	public void itGetsDirectoryFromCommandLineArgs() {
		String[] args = { "-p", "5000", "-d", "meagan" };
		parser = new Parser(args);
		assertEquals("meagan", parser.directory());
	}
	
	@Test
	public void itGetsDirectoryWithOutPortFlag() {
		String[] args = { "-d", "meagan" };
		parser = new Parser(args);
		assertEquals("meagan", parser.directory());
	}
	
	@Test
	public void itDefaultsToPublic() {
		String[] args = new String[0];
		parser = new Parser(args);
		assertEquals("/Users/meaganewaller/apprenticeship/javaserver/cob_spec", parser.directory());
	}
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void itDoesntAllowPortsBelow1024() {
		expectedException.expect(RuntimeException.class);
		expectedException.expectMessage("Pick a port 1024 or greater, please!");
		String[]args = { "-p", "500" };
		parser = new Parser(args);
		parser.portNumber();
	}


}
